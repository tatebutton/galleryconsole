﻿using System;
using System.IO;
using Microsoft.Extensions.Configuration;

namespace PhotoGallery
{
    class Program
    {
        public static IConfigurationRoot Configuration { get; set; }
        static void Main(string[] args)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json");

            Configuration = builder.Build();

            var svc = new PhotoService.Services.PhotoService(Configuration["jsonplaceholderRootURL"]);
            
            while (true)
            {
                Console.WriteLine("Enter AlbumId to view available photos:");
                
                var response = svc.ValidateResponse(Console.ReadLine());

                if (response.Success)
                {
                    var photos = svc.GetPhotosByAlbumId(response.AlbumId);
                    if (photos.Count == 0)
                    {
                        Console.WriteLine("No photos in album");
                    }
                    foreach (var photo in photos)
                    {
                        Console.WriteLine($"[{photo.Id}] - {photo.Title}");
                    }
                }
                else
                {
                    Console.WriteLine(response.Response);
                }
                
                    
                
                
                
                
            }

            

        }
    }
}