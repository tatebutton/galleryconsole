using System.Collections.Generic;

namespace PhotoService.Models
{
    public class ValidationResponse
    {
        public string Response => Success ? "Input is valid" : "Input is invalid";

        public int AlbumId { get; set; }
        public bool Success { get; set; }

    }
}