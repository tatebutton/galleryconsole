﻿namespace PhotoService.Services
{
    public interface IJsonPlaceholderService
    {
        string RequestAlbum(int albumId);
    }
}