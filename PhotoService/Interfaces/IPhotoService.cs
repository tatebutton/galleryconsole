﻿using System.Collections.Generic;
using PhotoService.Models;

namespace PhotoService.Services
{
    public interface IPhotoService
    {
        List<Photo> GetPhotosByAlbumId(int albumId);
        ValidationResponse ValidateResponse(string albumIdStr);
    }
}