﻿using System;
using System.Net;
using System.Net.Http;

namespace PhotoService.Services
{
    public class JsonPlaceholderService : IJsonPlaceholderService
    {
        private readonly string _connectionString;
        public JsonPlaceholderService(string connectionString)
        {
            _connectionString = connectionString;
        }

        public string RequestAlbum(int albumId)
        {
           
            using (var client = new HttpClient(new HttpClientHandler { AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate }))
            {
                client.BaseAddress = new Uri(_connectionString);
                HttpResponseMessage response = client.GetAsync($"?albumId={albumId}").Result;
                response.EnsureSuccessStatusCode();
                return  response.Content.ReadAsStringAsync().Result;
                
            }
            
        }


    }
}
