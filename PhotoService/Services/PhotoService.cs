﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using PhotoService.Models;

namespace PhotoService.Services
{
    public class PhotoService : IPhotoService
    {
        private readonly string _connectionString;
        public PhotoService(string connectionString)
        {
            _connectionString = connectionString;
        }
        public List<Photo> GetPhotosByAlbumId(int albumId)
        {
            var svc = new JsonPlaceholderService(_connectionString);
            var request = svc.RequestAlbum(albumId);
            var photoList = JsonConvert.DeserializeObject<List<Photo>>(request);
            return photoList;
        }

        public ValidationResponse ValidateResponse(string albumIdStr)
        {
            try
            {
                int albumId = Convert.ToInt32(albumIdStr);
                return new ValidationResponse() {AlbumId = albumId, Success = true};

            }
            catch (Exception e)
            {
                return new ValidationResponse() {Success = false};
            }
        }
    }
}
