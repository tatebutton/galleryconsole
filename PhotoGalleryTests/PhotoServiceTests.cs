using System;
using System.Linq;
using System.Net.Http;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PhotoService.Services;

namespace PhotoGalleryTests
{
    [TestClass]
    public class PhotoServiceTests
    {
        

        //Integration Test
        [TestMethod]
        public void ValidatePhotosOfDefinedAlbum()
        {
            int photoAlbumId = 3;
            var svc = new PhotoService.Services.PhotoService("https://jsonplaceholder.typicode.com/photos");
            var photoList = svc.GetPhotosByAlbumId(photoAlbumId);
            var photoListAlbumId = photoList.FirstOrDefault().AlbumId;
            Assert.AreEqual(photoAlbumId,(int) photoListAlbumId);
        }

        [TestMethod]
        [ExpectedException(typeof(AggregateException))]
        public void ValidatePhotosOfDefinedAlbumaa()
        {
            int photoAlbumId = 3;
            var svc = new PhotoService.Services.PhotoService("https://fdsafda.co");
            svc.GetPhotosByAlbumId(photoAlbumId);
        }

        [TestMethod]
        public void ValidateResponseShouldReturnSuccessTrue()
        {
            var svc = new PhotoService.Services.PhotoService("https://jsonplaceholder.typicode.com/photos");
            Assert.AreEqual(svc.ValidateResponse("3").Success,true);
        }

        [TestMethod]
        public void ValidateResponseShouldReturnSuccessFalse()
        {
            var svc = new PhotoService.Services.PhotoService("https://jsonplaceholder.typicode.com/photos");
            Assert.AreEqual(svc.ValidateResponse("f").Success, false);
        }


    }
}
 